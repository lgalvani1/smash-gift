# smash_gift

This project is a flutter mobile app  following the fullstack developer case for Smash admission process.

## Getting Started

A few resources to get you started if this is your first running this project.

- [Firebase](https://firebase.google.com/)
- [Firebase functions](https://firebase.google.com/docs/functions/)
- [Firebase firestore](https://firebase.google.com/docs/firestore/)
- [Firebase storage](https://firebase.google.com/docs/storage/)

### Back-end node.js

Firebase storage receives de CSV file and trigger the back-end(Firebase functions), that will read the uploaded file and create the table on Firebase Firestore.

### Front-end architecture

This project is following the Clean architecture principles.