import 'package:equatable/equatable.dart';

class WorldCitiesEntity with EquatableMixin {
  WorldCitiesEntity({
    required this.name,
    required this.country,
    required this.subcountry,
    required this.geonameid,
  });

  String name;
  String country;
  String subcountry;
  int geonameid;

  @override
  List<Object?> get props => [name, country, subcountry, geonameid];
}
