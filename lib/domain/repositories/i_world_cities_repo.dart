import 'package:dartz/dartz.dart';
import 'package:smash_gift/domain/entities/world_cities_entity.dart';
import 'package:smash_gift/domain/errors/world_cities_exception.dart';

abstract class IWorldCitiesRepo {
  Future<Either<WorldCitiesException, List<WorldCitiesEntity>>>
      getAllCoutries();

  Future<Either<WorldCitiesException, List<WorldCitiesEntity>>> getAllCities(
      String country);
}
