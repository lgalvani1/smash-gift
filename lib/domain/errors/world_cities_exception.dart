class WorldCitiesException<T> implements Exception {
  const WorldCitiesException(this.extra, {this.exception});

  final T extra;

  final Exception? exception;

  @override
  String toString() => '$extra$_separator$_exceptionMessage';

  String get _separator => exception != null ? '\n' : '';

  String get _exceptionMessage => exception?.toString() ?? '';
}
