import 'package:dartz/dartz.dart';
import 'package:smash_gift/domain/errors/world_cities_exception.dart';
import 'package:smash_gift/domain/repositories/i_world_cities_repo.dart';

abstract class IGetAllCoutriesUsecase {
  Future<Either<WorldCitiesException, List<WorldCountriesDto>>> call();
}

class GetAllCoutriesUsecaseImpl implements IGetAllCoutriesUsecase {
  const GetAllCoutriesUsecaseImpl(this._repo);

  final IWorldCitiesRepo _repo;

  @override
  Future<Either<WorldCitiesException, List<WorldCountriesDto>>> call() async {
    var countries = await _repo.getAllCoutries();

    return countries.fold(
      (l) => Left(l),
      (r) => Right(
        r.map((e) => WorldCountriesDto(country: e.country)).toList(),
      ),
    );
  }
}

class WorldCountriesDto {
  WorldCountriesDto({
    required this.country,
  });

  final String country;
}
