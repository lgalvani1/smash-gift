import 'package:dartz/dartz.dart';
import 'package:smash_gift/domain/errors/world_cities_exception.dart';
import 'package:smash_gift/domain/repositories/i_world_cities_repo.dart';

abstract class IGetAllCitiesUsecase {
  Future<Either<WorldCitiesException, List<WorldCitiesDto>>> call(
      String country);
}

class GetAllCitiesUsecaseImpl implements IGetAllCitiesUsecase {
  const GetAllCitiesUsecaseImpl(this._repo);

  final IWorldCitiesRepo _repo;

  @override
  Future<Either<WorldCitiesException, List<WorldCitiesDto>>> call(
      String country) async {
    var cities = await _repo.getAllCities(country);

    return cities.fold(
      (l) => Left(l),
      (r) => Right(
        r.map((e) => WorldCitiesDto(city: e.subcountry)).toList(),
      ),
    );
  }
}

class WorldCitiesDto {
  WorldCitiesDto({
    required this.city,
  });

  final String city;
}
