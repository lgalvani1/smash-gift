import 'package:mobx/mobx.dart';
import 'package:smash_gift/domain/usecases/get_all_coutries_usecase.dart';

part 'world_countries_store.g.dart';

class WorldCountriesStore = WorldCountriesStoreBase with _$WorldCountriesStore;

abstract class WorldCountriesStoreBase with Store {
  WorldCountriesStoreBase(
    this._getAllCoutriesUsecase,
  );

  final IGetAllCoutriesUsecase _getAllCoutriesUsecase;

  @readonly
  List<WorldCountriesDto> _coutries = [];

  @action
  Future<void> loadCoutries() async {
    final v = await _getAllCoutriesUsecase();
    v.fold((l) => l, (r) {
      _coutries = r.toList();
    });
  }
}
