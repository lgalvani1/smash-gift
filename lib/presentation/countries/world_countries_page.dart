import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:smash_gift/domain/usecases/get_all_coutries_usecase.dart';
import 'package:smash_gift/presentation/countries/world_countries_store.dart';

class WorldCountriesPage extends StatefulWidget {
  const WorldCountriesPage({
    super.key,
    required this.store,
  });

  final WorldCountriesStore store;

  @override
  State<WorldCountriesPage> createState() => _WorldCountriesPageState();
}

class _WorldCountriesPageState extends State<WorldCountriesPage> {
  WorldCountriesStore get _store => widget.store;

  List<WorldCountriesDto> get _countries => _store.coutries;

  @override
  void initState() {
    super.initState();
    _store.loadCoutries();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Observer(
        builder: (_) => ListView.builder(
          itemCount: _countries.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(_countries[index].country),
              trailing: const Icon(Icons.chevron_right),
              onTap: () {
                Modular.to.navigate("/cities/${_countries[index].country}");
              },
            );
          },
        ),
      ),
    );
  }
}
