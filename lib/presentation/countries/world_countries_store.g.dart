// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'world_countries_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$WorldCountriesStore on WorldCountriesStoreBase, Store {
  late final _$_coutriesAtom =
      Atom(name: 'WorldCountriesStoreBase._coutries', context: context);

  List<WorldCountriesDto> get coutries {
    _$_coutriesAtom.reportRead();
    return super._coutries;
  }

  @override
  List<WorldCountriesDto> get _coutries => coutries;

  @override
  set _coutries(List<WorldCountriesDto> value) {
    _$_coutriesAtom.reportWrite(value, super._coutries, () {
      super._coutries = value;
    });
  }

  late final _$loadCoutriesAsyncAction =
      AsyncAction('WorldCountriesStoreBase.loadCoutries', context: context);

  @override
  Future<void> loadCoutries() {
    return _$loadCoutriesAsyncAction.run(() => super.loadCoutries());
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
