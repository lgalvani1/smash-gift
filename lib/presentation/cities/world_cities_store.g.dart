// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'world_cities_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$WorldCitiesStore on WorldCitiesStoreBase, Store {
  late final _$_citiesAtom =
      Atom(name: 'WorldCitiesStoreBase._cities', context: context);

  List<WorldCitiesDto> get cities {
    _$_citiesAtom.reportRead();
    return super._cities;
  }

  @override
  List<WorldCitiesDto> get _cities => cities;

  @override
  set _cities(List<WorldCitiesDto> value) {
    _$_citiesAtom.reportWrite(value, super._cities, () {
      super._cities = value;
    });
  }

  late final _$loadCitiesAsyncAction =
      AsyncAction('WorldCitiesStoreBase.loadCities', context: context);

  @override
  Future<void> loadCities(String country) {
    return _$loadCitiesAsyncAction.run(() => super.loadCities(country));
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
