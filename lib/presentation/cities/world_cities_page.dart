import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:smash_gift/domain/entities/world_cities_entity.dart';
import 'package:smash_gift/domain/usecases/get_all_cities_usacase.dart';
import 'package:smash_gift/presentation/cities/world_cities_store.dart';

class WorldCitiesPage extends StatefulWidget {
  const WorldCitiesPage(
      {super.key, required this.store, required this.country});

  final WorldCitiesStore store;
  final String country;

  @override
  State<WorldCitiesPage> createState() => _WorldCitiesPageState();
}

class _WorldCitiesPageState extends State<WorldCitiesPage> {
  WorldCitiesStore get _store => widget.store;
  String get _country => widget.country;
  List<WorldCitiesDto> get _cities => _store.cities;

  @override
  void initState() {
    super.initState();
    _store.loadCities(_country);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          iconSize: 20.0,
          onPressed: () => Modular.to.navigate("/"),
        ),
        centerTitle: true,
      ),
      body: Observer(
        builder: (_) => ListView.builder(
          itemCount: _cities.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(_cities[index].city),
            );
          },
        ),
      ),
    );
  }
}
