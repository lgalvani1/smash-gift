import 'package:mobx/mobx.dart';
import 'package:smash_gift/domain/usecases/get_all_cities_usacase.dart';

part 'world_cities_store.g.dart';

class WorldCitiesStore = WorldCitiesStoreBase with _$WorldCitiesStore;

abstract class WorldCitiesStoreBase with Store {
  WorldCitiesStoreBase(
    this._getAllCitiesUsecase,
  );

  final IGetAllCitiesUsecase _getAllCitiesUsecase;

  @readonly
  List<WorldCitiesDto> _cities = [];

  @action
  Future<void> loadCities(String country) async {
    final v = await _getAllCitiesUsecase(country);
    v.fold((l) => l, (r) => _cities = r.toList());
  }
}
