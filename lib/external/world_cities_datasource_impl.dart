import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:smash_gift/data/datasources/i_world_cities_datasource.dart';
import 'package:smash_gift/data/models/world_cities_model.dart';
import 'package:smash_gift/domain/entities/world_cities_entity.dart';

class WorldCitiesDataSourceImpl implements IWorldCitiesDataSource {
  FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<List<WorldCitiesEntity>> getAllCities(String country) async {
    CollectionReference collection = db.collection("world-cities");

    QuerySnapshot snapshot =
        await collection.where("country", isEqualTo: country).get();

    List<WorldCitiesModel> result = snapshot.docs
        .map((doc) => WorldCitiesModel.fromFirestore(doc))
        .toList();

    return result;
  }

  @override
  Future<List<WorldCitiesEntity>> getAllCoutries() async {
    CollectionReference collection = db.collection("world-countries");

    QuerySnapshot snapshot = await collection.orderBy("country").get();

    List<WorldCitiesModel> result = snapshot.docs
        .map((doc) => WorldCitiesModel.fromFirestore(doc))
        .toList();

    return result;
  }
}
