import 'package:smash_gift/domain/entities/world_cities_entity.dart';

abstract class IWorldCitiesDataSource {
  Future<List<WorldCitiesEntity>> getAllCoutries();

  Future<List<WorldCitiesEntity>> getAllCities(String country);
}
