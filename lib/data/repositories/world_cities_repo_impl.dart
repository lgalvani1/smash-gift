import 'package:dartz/dartz.dart';
import 'package:smash_gift/data/datasources/i_world_cities_datasource.dart';
import 'package:smash_gift/domain/entities/world_cities_entity.dart';
import 'package:smash_gift/domain/errors/world_cities_exception.dart';
import 'package:smash_gift/domain/repositories/i_world_cities_repo.dart';

class WorldCitiesRepoImpl extends IWorldCitiesRepo {
  WorldCitiesRepoImpl(
    this._dataSource,
  );

  final IWorldCitiesDataSource _dataSource;

  @override
  Future<Either<WorldCitiesException, List<WorldCitiesEntity>>> getAllCities(
      String country) async {
    try {
      final result = await _dataSource.getAllCities(country);

      return Right(result);
    } on Exception catch (ex) {
      return Left(WorldCitiesException('Repo exception', exception: ex));
    }
  }

  @override
  Future<Either<WorldCitiesException, List<WorldCitiesEntity>>>
      getAllCoutries() async {
    try {
      final result = await _dataSource.getAllCoutries();

      return Right(result);
    } on Exception catch (ex) {
      return Left(WorldCitiesException('Repo exception', exception: ex));
    }
  }
}
