import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:smash_gift/domain/entities/world_cities_entity.dart';

class WorldCitiesModel extends WorldCitiesEntity {
  WorldCitiesModel({
    required String name,
    required String country,
    required String subcountry,
    required int geonameid,
  }) : super(
            name: name,
            country: country,
            subcountry: subcountry,
            geonameid: geonameid);

  WorldCitiesEntity toWorldCitiesEntity() {
    return WorldCitiesEntity(
        name: name,
        country: country,
        subcountry: subcountry,
        geonameid: geonameid);
  }

  WorldCitiesModel.fromFirestore(QueryDocumentSnapshot doc)
      : super(
            name: doc['name'],
            country: doc['country'],
            subcountry: doc['subcountry'],
            geonameid: int.parse(doc['geonameid']));
}
