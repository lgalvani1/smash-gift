import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:smash_gift/data/datasources/i_world_cities_datasource.dart';
import 'package:smash_gift/data/repositories/world_cities_repo_impl.dart';
import 'package:smash_gift/domain/repositories/i_world_cities_repo.dart';
import 'package:smash_gift/domain/usecases/get_all_cities_usacase.dart';
import 'package:smash_gift/domain/usecases/get_all_coutries_usecase.dart';
import 'package:smash_gift/external/world_cities_datasource_impl.dart';
import 'package:smash_gift/firebase_options.dart';
import 'package:smash_gift/presentation/cities/world_cities_page.dart';
import 'package:smash_gift/presentation/countries/world_countries_page.dart';
import 'package:smash_gift/presentation/cities/world_cities_store.dart';
import 'package:smash_gift/presentation/countries/world_countries_store.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  runApp(ModularApp(module: AppModule(), child: const AppWidget()));
}

class AppWidget extends StatelessWidget {
  const AppWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'World cities',
      theme: ThemeData(primarySwatch: Colors.blue),
      routeInformationParser: Modular.routeInformationParser,
      routerDelegate: Modular.routerDelegate,
    );
  }
}

class AppModule extends Module {
  @override
  List<Bind> get binds => [
        // Data Source
        Bind.lazySingleton((i) => WorldCitiesDataSourceImpl()),

        // Repository
        Bind.lazySingleton(
            (i) => WorldCitiesRepoImpl(i<IWorldCitiesDataSource>())),

        // Usecases
        Bind.lazySingleton(
            (i) => GetAllCoutriesUsecaseImpl(i<IWorldCitiesRepo>())),

        Bind.lazySingleton(
            (i) => GetAllCitiesUsecaseImpl(i<IWorldCitiesRepo>())),

        // MobX store
        Bind.lazySingleton((i) => WorldCountriesStore(
              i<IGetAllCoutriesUsecase>(),
            )),
        Bind.lazySingleton((i) => WorldCitiesStore(
              i<IGetAllCitiesUsecase>(),
            )),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(
          Modular.initialRoute,
          child: (context, args) =>
              WorldCountriesPage(store: Modular.get<WorldCountriesStore>()),
        ),
        ChildRoute(
          "/cities/:country",
          child: (context, args) => WorldCitiesPage(
            store: Modular.get<WorldCitiesStore>(),
            country: args.params["country"],
          ),
        ),
      ];
}
