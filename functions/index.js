/* eslint-disable linebreak-style */
/* eslint-disable require-jsdoc */
"use strict";

const functions = require("firebase-functions");
const admin = require("firebase-admin");
const {Storage} = require("@google-cloud/storage");
const parser = require("csv-parser");
admin.initializeApp();

/**
 * Listen for object changes on the default Cloud Storage bucket.
 */
exports.worldCities = functions.region("southamerica-east1")
    .storage
    .object()
    .onFinalize(async (file) => {
      const fileName = file.name;

      if (!fileName.endsWith(".csv")) {
        console.log("Not a .csv file, ignoring.");
        return;
      }

      const data = await readCSVContent(file);
      await createWorldCountries(data);
      await createWorldCities(data);

      return;
    });

async function readCSVContent(file) {
  return new Promise((resolve, reject) => {
    const storage = new Storage();
    const results = [];
    storage.bucket(file.bucket)
        .file(file.name)
        .createReadStream()
        .pipe(parser({delimiter: ",", from_line: 2}))
        .on("error", (err) => {
          reject(err);
        })
        .on("data", (chunk) => {
          console.log(`CHUNK => ${JSON.stringify(chunk)}`);
          results.push(chunk);
        })
        .on("end", () => {
          resolve(results);
        });
  });
}

async function createWorldCountries(file) {
  const countries = [...new Map(file.map((m) => [m.country, m])).values()];

  const writeBatch = admin.firestore().batch();

  countries.map( (object) => {
    const ref = admin.firestore()
        .collection("world-countries")
        .doc();

    writeBatch.set(ref, object);
  });

  writeBatch.commit( ).then((results) => {
    console.log(`Countries added: ${results.length}`);
  }).catch((error) => {
    console.log(`Error adding coutries: ${error}` );
  });
}

async function createWorldCities(file) {
  const writeBatch = admin.firestore().batch();

  file.map( (object) => {
    const ref = admin.firestore()
        .collection("world-cities")
        .doc();

    writeBatch.set(ref, object);
  });

  writeBatch.commit( ).then((results) => {
    console.log(`Cities added: ${results.length}`);
  }).catch((error) => {
    console.log(`Error adding cities: ${error}` );
  });
}
